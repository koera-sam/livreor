<?php

/**
 * Created by PhpStorm.
 * User: KOERA
 * Date: 24/07/2017
 * Time: 15:16
 */
class Livreor_commentaires_model extends CI_Model
{
    private $id;
    private $pseudo;
    private $message;
    private $date;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    public function createComment($pseudo,$message){
        $data = array('pseudo'=>$pseudo,'message'=>$message,'date'=>date("Y-m-d h:i:s"));
        $this->db->insert('livreor_commentaires',$data);
        return true;
    }

    public function getAllComment(){
        $this->db->select('*');
        $this->db->from('livreor_commentaires');
        $this->db->order_by("date", "asc");
        $query = $this->db->get();
        return $query->result();
    }

}