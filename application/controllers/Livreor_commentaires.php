<?php

/**
 * Created by PhpStorm.
 * User: KOERA
 * Date: 24/07/2017
 * Time: 15:35
 */
class Livreor_commentaires extends CI_Controller
{
    public function listCommentaire(){
        $this->load->helper('form');
        $this->load->model('Livreor_commentaires_model');
        $comments = $this->Livreor_commentaires_model->getAllComment();
        $data = array();
        $data['comments'] = $comments;
        $this->load->view('welcome_message',$data);
    }
    public function addComment(){
        $data = array(
            'pseudo' => $this->input->post('pseudo'),
            'commentaire' => $this->input->post('comment')
        );
        $this->load->model('Livreor_commentaires_model');
        $comment = $this->Livreor_commentaires_model->createComment($data['pseudo'],$data['commentaire']);
        $this->load->helper('form');
        $this->load->model('Livreor_commentaires_model');
        $comments = $this->Livreor_commentaires_model->getAllComment();
        $data = array();
        $data['comments'] = $comments;
        $this->load->view('welcome_message',$data);
    }
}