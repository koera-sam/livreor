<?php

/**
 * Created by PhpStorm.
 * User: KOERA
 * Date: 25/07/2017
 * Time: 08:31
 */
class Livreor_service extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function getAllComment(){
        $this->load->model('Livreor_commentaires_model');
        $comments = $this->Livreor_commentaires_model->getAllComment();
        $data = array();
        $i = 0;
        foreach ($comments as $k){
            $data[$i] = array("pseudo"=>$k->pseudo,
                "message"=>$k->message,
                "date"=>$k->date);
            $i++;
        }
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(201)
            ->set_output(json_encode($data));
    }
}